<?php

namespace mindplay\capital;

/**
 * This interface defines a means of expanding an asset name
 * into a full filename, into a URL and an absolute local path.
 */
interface AssetLocator
{
    /**
     * @param string $base_name base asset name (e.g. without file extension)
     *
     * @return string full asset name (including e.g. file extension/suffix)
     */
    public function getName($base_name);

    /**
     * @param string $name asset name
     *
     * @return string asset URL
     */
    public function getURL($name);

    /**
     * @param string $name asset name
     *
     * @return string local asset path
     */
    public function getPath($name);
}
