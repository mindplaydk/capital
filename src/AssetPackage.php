<?php

namespace mindplay\capital;

/**
 * This class represents a list of packaged assets - this is intended
 * primarily for offline use, e.g. to produce a configuration file
 * for an external JS/CSS serialization/minification tool.
 */
class AssetPackage
{
    /**
     * @var string package name
     */
    public $name;

    /**
     * @var string local package path
     */
    public $path;

    /**
     * @var string[] list of source files belonging to this package
     */
    public $sources = array();
}
