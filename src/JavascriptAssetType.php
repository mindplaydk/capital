<?php

namespace mindplay\capital;

/**
 * Javascript asset type (emits a script-tag)
 */
class JavascriptAssetType implements AssetType
{
    public function renderAsset($url)
    {
        return '<script type="text/javascript" src="' . htmlspecialchars($url) . '"></script>';
    }
}
