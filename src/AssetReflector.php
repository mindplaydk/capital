<?php

namespace mindplay\capital;

use ReflectionClass;
use ReflectionMethod;

/**
 * This utility class provides some reflection methods intended primarily
 * for offline use, e.g. to produce a configuration file for an external
 * JS/CSS serialization/minification tool.
 */
class AssetReflector
{
    /**
     * Invoke every public method of a given asset manager matching a
     * given method name mask, causing it to add every known asset to it's
     * internal asset lists.
     *
     * @param object $asset_manager userland asset manager
     * @param string $method_mask   method name mask (defaults to "use*")
     *
     * @return void
     */
    public function loadAll($asset_manager, $method_mask = 'use*')
    {
        $class = new ReflectionClass($asset_manager);

        foreach ($class->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
            if ($method->isStatic()) {
                continue; // skip static methods
            }

            if ($method->getNumberOfRequiredParameters() > 0) {
                continue; // skip methods requiring arguments
            }

            if (false === fnmatch($method_mask, $method->name)) {
                continue; // skip methods not matching the given mask
            }

            $method->invoke($asset_manager);
        }
    }

    /**
     * Group or more lists of assets (of the same type) by package name.
     *
     * @param AssetList ...$list one or more asset lists
     *
     * @return AssetPackage[] list of packages in the given asset list
     */
    public function getPackages()
    {
        /**
         * @var AssetList[] $lists
         */
        $lists = func_get_args();

        // TODO combine assets into packages
    }
}
