<?php

namespace mindplay\capital;

/**
 * Stylesheet asset type (emits a link-tag)
 */
class StylesheetAssetType implements AssetType
{
    public function renderAsset($url)
    {
        return '<link rel="stylesheet" type="text/css" href="' . $url . '"/>';
    }
}
