<?php

namespace mindplay\capital;

/**
 * This class represents a set of assets mapped to package names
 */
class AssetList
{
    /**
     * @var string[] map where asset name => package name
     */
    private $map = array();

    /**
     * @var AssetType asset type
     */
    private $type;

    /**
     * @var AssetLocator asset locator
     */
    private $asset_locator;

    /**
     * @var AssetLocator packaged asset locator
     */
    private $package_locator;

    /**
     * @param AssetType    $type            asset type
     * @param AssetLocator $asset_locator   asset locator
     * @param AssetLocator $package_locator packaged asset locator
     */
    public function __construct(AssetType $type, AssetLocator $asset_locator, AssetLocator $package_locator)
    {
        $this->type = $type;
        $this->asset_locator = $asset_locator;
        $this->package_locator = $package_locator;
    }

    /**
     * @param string $name    asset name (relative asset path/filename)
     * @param string $package package name (optional; non-packaged assets are considered packages)
     */
    public function add($name, $package = null)
    {
        $this->map[$name] = $package;
    }

    /**
     * In a development environment, you most likely want to embed all of
     * the assets without packaging anything.
     *
     * @return string rendered HTML tags to embed the assets
     */
    public function getAssetTags()
    {
        return implode(
            "\n",
            array_map(
                array($this->type, 'renderAsset'),
                $this->getAssetURLs()
            )
        );
    }

    /**
     * In a production environment, you most likely want to embed your
     * assets as packages to reduce the number of hits on the server.
     *
     * @return string rendered HTML tags to embed the packages
     */
    public function getPackageTags()
    {
        return implode(
            "\n",
            array_map(
                array($this->type, 'renderAsset'),
                $this->getPackageURLs()
            )
        );
    }

    /**
     * @return string[] list of asset URLs
     */
    public function getAssetURLs()
    {
        return $this->locateAssets('getURL');
    }

    /**
     * @return string[] list of local asset paths
     */
    public function getAssetPaths()
    {
        return $this->locateAssets('getPath');
    }

    /**
     * @return string[] list of package URLs
     */
    public function getPackageURLs()
    {
        return $this->locatePackages('getURL');
    }

    /**
     * @return string[] list of local package paths
     */
    public function getPackagePaths()
    {
        return $this->locatePackages('getPath');
    }

    /**
     * @param string $method asset locator method name ("getURL" or "getPath")
     *
     * @return string[] list of asset names
     */
    private function locateAssets($method)
    {
        return array_map(
            array($this->asset_locator, $method),
            array_map(
                array($this->asset_locator, 'getName'),
                array_keys($this->map)
            )
        );
    }

    /**
     * @param string $method asset locator method name ("getURL" or "getPath")
     *
     * @return string[] list of package names (includes un-packaged assets, which are considered packages)
     */
    private function locatePackages($method)
    {
        return array_merge(
            array_map(
                array($this->package_locator, $method),
                array_map(
                    array($this->package_locator, 'getName'),
                    array_unique(array_values(array_filter($this->map)))
                )
            ),
            array_map(
                array($this->asset_locator, $method),
                array_map(
                    array($this->asset_locator, 'getName'),
                    array_keys(array_filter($this->map, 'is_null'))
                )
            )
        );
    }
}
