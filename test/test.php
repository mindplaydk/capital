<?php

require __DIR__ . '/header.php';
require __DIR__ . '/fixtures.php';

header('Content-type: text/plain');

if (coverage()) {
    $filter = coverage()->filter();

    // whitelist the files to cover:

    $filter->addDirectoryToWhitelist(dirname(__DIR__) . '/src');

    coverage()->setProcessUncoveredFilesFromWhitelist(true);

    // start code coverage:

    coverage()->start('test');
}

test(
    'Creates script and link tags',
    function () {
        $manager = new MyAssetManager();

        $manager->useCalendar();
        $manager->useArticleEditor();

        $expected_js_tags = implode(
            "\n",
            array(
                '<script type="text/javascript" src="/assets/articles/jquery.js"></script>',
                '<script type="text/javascript" src="/assets/articles/jquery-calendar.js"></script>',
                '<script type="text/javascript" src="/assets/articles/article-editor.js"></script>',
            )
        );

        $expected_package_tags = implode(
            "\n",
            array(
                '<script type="text/javascript" src="/assets/ui.min.js"></script>',
                '<script type="text/javascript" src="/assets/articles/article-editor.js"></script>',
            )
        );

        $expected_css_tag = '<link rel="stylesheet" type="text/css" href="/assets/articles/calendar.css"/>';

        ok($manager->js->getAssetTags() === $expected_js_tags, 'emits expected script-tags');
        ok($manager->js->getPackageTags() === $expected_package_tags, 'emits expected package script-tags');
        ok($manager->css->getAssetTags() === $expected_css_tag, 'emits expected link-tag');
    }
);

if (coverage()) {
    // stop code coverage:

    coverage()->stop();

    // output code coverage report to console:

    $report = new PHP_CodeCoverage_Report_Text(10, 90, false, false);

    echo $report->process(coverage(), false);

    // output code coverage report for integration with CI tools:

    $report = new PHP_CodeCoverage_Report_Clover();

    $report->process(coverage(), __DIR__ . '/build/logs/clover.xml');
}

exit(status());
