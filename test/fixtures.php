<?php

use mindplay\capital\AssetList;
use mindplay\capital\AssetLocator;
use mindplay\capital\JavascriptAssetType;
use mindplay\capital\StylesheetAssetType;

/**
 * Sample asset locator demonstrating assets with public URLs that differ
 * from the local paths of the physical assets, e.g. assets with URLs that
 * are being rewritten, for example using .htaccess rewrites.
 */
class MyAssetLocator implements AssetLocator
{
    /**
     * @var string module name
     */
    private $module_name;

    public function __construct($module_name)
    {
        $this->module_name = $module_name;
    }

    public function getName($base_name)
    {
        return $base_name;
    }

    public function getURL($name)
    {
        return "/assets/{$this->module_name}/{$name}";
    }

    public function getPath($name)
    {
        return "/modules/{$this->module_name}/assets/{$name}";
    }
}

/**
 * Sample package locator demonstrating filenames being transformed
 * before getting emitted, in this case adding ".min.js" to package names.
 */
class MyPackageLocator implements AssetLocator
{
    public function getName($base_name)
    {
        return "{$base_name}.min.js";
    }

    public function getURL($name)
    {
        return "/assets/{$name}";
    }

    public function getPath($name)
    {
        return "/assets/{$name}";
    }
}

/**
 * Your own asset manager can use as many asset lists as needed for
 * your purposes - this example uses just two; in a real world scenario
 * it's common to have e.g. a JS asset list for assets embedded in the
 * document head, and a separate list for scripts before end of body.
 */
class MyAssetManager
{
    /**
     * @var AssetList
     */
    public $js;

    /**
     * @var AssetList
     */
    public $css;

    public function __construct()
    {
        $js_type = new JavascriptAssetType();
        $css_type = new StylesheetAssetType();

        $asset_locator = new MyAssetLocator('articles');
        $package_locator = new MyPackageLocator();

        $this->js = new AssetList($js_type, $asset_locator, $package_locator);
        $this->css = new AssetList($css_type, $asset_locator, $package_locator);
    }

    public function useJQuery()
    {
        $this->js->add('jquery.js', 'ui');
    }

    public function useCalendar()
    {
        $this->useJQuery();
        $this->js->add('jquery-calendar.js', 'ui');
        $this->css->add('calendar.css');
    }

    public function useArticleEditor()
    {
        $this->useJQuery();
        $this->js->add('article-editor.js');
    }
}
